<?php
$data = [
[
    'name' => 'Gewurztraminer Turckheim 2006, Domaine Zind-Humbrecht',
    'price' => 120
],
[
    'name' => 'Riesling Trocken Erdener Treppchen Grand Cru 2008, Dr. Loosen',
    'price' => 100
],
[
    'name' => 'Amarone della Valpolicella Classico DOC 2006',
    'price' => 200
],
[
    'name' => 'Quimera 2006, Achaval Ferrer',
    'price' => 220
],
[
    'name' => 'The Dead Arm Shiraz 2006',
    'price' => 300
],
[
    'name' => 'Pouilly-Fuisse Domaine Ferret 2008, Louis Jadot',
    'price' => 240
],
[
    'name' => 'Savigny-Les-Beaune Narbantons 2002, Louis Jadot',
    'price' => 70
],
[
    'name' => 'Perrin & Fils Chateauneuf-du-Pape Les Sinards 2006',
    'price' => 90
],
[
    'name' => 'Greppone Mazzi Brunello di Montalcino Riserva 2003, Ruffino',
    'price' => 120
],
[
    'name' => 'Chablis Grand Cru Grenouille 2008, Droinы',
    'price' => 150
]
];
$user_cart = NULL;
if(!empty($_COOKIE['user_cart'])){
    $user_cart = json_decode($_COOKIE['user_cart'], true);
};

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link href="style.css" rel="stylesheet">
    <meta name="Description" content="learning $_COOKIE">
    <title>Wine Story</title>
</head>
<body>
    <div class="container col-md-7">
    <h1>Top 10 best foreign wines</h1>
    <hr>
        <h2>Buy the best wine for the holidays</h2>
        <table class="table table-success">
            <thead>
                <tr class="table-active">
                    <th scope="col">#</th>
                    <th scope="col">Product Name</th>
                    <th scope="col">Price</th>
                    <th scope="col">Buy</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($data as $key => $product):?>
                    <tr>
                        <th scope="row"><?= ++$key; ?></th> 
                        <td><?= $product['name']; ?></td>
                        <td><?= $product['price']; ?></td>
                        <td><a class="btn btn-primary" href="cart_add.php?buy_item=<?=$key;?>">Buy</a></td>
                    </tr>
                <?php endforeach; ?>   
            </tbody>
        </table>
    </div> 
    <div class="container col-md-3 ">
    <?php if(empty($user_cart)):?>
        <h2>Just leave your purchases here!</h2>
        <?php else:?>
        <h2>Product added successfully!!!</h2>
        <table class="table table table-dark">
            <thead>
                <tr class="table-active">
                    <th scope="col">Name</th>
                    <th scope="col">Amount</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($user_cart as $item => $amount):?>   
                    <tr>
                        <td><?=$data[--$item]['name'];?></td>
                        <td><?=$amount;?></td>
                    </tr>
                <?php endforeach; ?>   
            </tbody>
        </table>
        <?php endif; ?>
    </div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>      
</body>
</html>